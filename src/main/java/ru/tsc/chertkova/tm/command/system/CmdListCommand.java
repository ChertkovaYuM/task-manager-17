package ru.tsc.chertkova.tm.command.system;

import ru.tsc.chertkova.tm.api.model.ICommand;
import ru.tsc.chertkova.tm.command.AbstractCommand;

import java.util.Collection;

public final class CmdListCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String DESCRIPTION = "Show commands list.";

    public static final String ARGUMENT = "-cmd";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand command : commands) {
            String name = command.getName();
            if (name != null && !name.isEmpty())
                System.out.println(command.getName());
        }
    }

}
